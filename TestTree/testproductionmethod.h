#ifndef TESTPRODUCTIONMETHOD_H
#define TESTPRODUCTIONMETHOD_H

#include "../TreeMaker/productionmethod.h"
#include "../TreeMaker/productionmethod.cpp"
#include <assert.h>
#include <iostream>

using namespace std;

bool testProductionMethod(){
    cout << endl << "TEST PRODUCTION METHOD START" << endl << endl;
    ProductionMethod* method = ProductionMethod::getInstance();
    if(method != NULL)
        return false;
    LAlphabet alphabet;
    alphabet.addNewSymbol('B',LKey::OLD_BRANCH);
    alphabet.addNewSymbol('L',LKey::NEW_BRANCH);
    alphabet.addNewSymbol('g',LKey::INCREASED_LENGHT);
    ProductionMethod::init('X',alphabet);
    method = ProductionMethod::getInstance();
    if(method == NULL)
    {
        return false;
    }
    if(method->getAxiom() != 'X')
    {
        return false;
    }
    method->addProductionString("X->I");

    //Product prod;
    std::string product;
    method->produce('X',product);
    if(product != "I")
    {
        cout << "Should produce \'I\'!" <<endl;
        return false;
    }
    Production prodX;
    prodX = method->produce('X');
    if(prodX.increaseLentgh != 0 && prodX.numOfNewBranches != 0 && prodX.numOfOldBranches !=0)
    {
        cout << "Should be empty!" <<endl;
        return false;
    }

    method->addProductionString("Y->X");
    method->produce('Y',product);
    if(product != "X")
    {
        cout << "Should produce \'X\'!" <<endl;
        return false;
    }
    Production prodY;
    prodY = method->produce('Y');
    if(prodY.increaseLentgh != 0 && prodY.numOfNewBranches != 0 && prodY.numOfOldBranches !=0)
    {
        cout << "prodX should be empty!" <<endl;
        return false;
    }

    method->addProductionString("I->L");
    method->produce('I',product);
    if(product != "L")
    {
        cout << "Should produce \'L\'!" <<endl;
        return false;
    }
    Production prodI;
    prodI = method->produce('I');
    if(prodI.increaseLentgh != 0 && prodI.numOfNewBranches != 1 && prodI.numOfOldBranches !=0)
    {
        cout << "Should be one new branch!" <<endl;
        return false;
    }

    std::cout << std::endl << std::endl;

    method->addProductionString("L->gBLL");
    Production prodL;
    prodL = method->produce('L');
    if(prodL.increaseLentgh != 1 && prodL.numOfNewBranches != 2 && prodL.numOfOldBranches !=1)
    {
        cout <<endl<< "Should receive 2 new, 1 old and 1 increase lenght!" <<endl;
        cout << "Received " << prodL.numOfNewBranches << " new, " << prodL.numOfOldBranches << " old, " << prodL.increaseLentgh << " increased lenght" << endl;
        return false;
    }
    std::string str;
    if(method->produce('Z',str))
    printf("Product from Z: ...%s...\n\n",str.c_str());
    //assert(method.produce("Z").c_str() != "Z" );
/*
    cout <<endl<< "int received from product X: " << method->produce('X') << endl;
    cout << "int received from product Y: " << method->produce('Y');
    cout << "int received from product I: " << method->produce('I') <<endl<<endl;

    cout << "Using ProduceString for: X, -1 returned: " << method->produceString("X",-1) << endl;
    cout << "Using ProduceString for: Y, 0 returned: " << method->produceString("Y",0) << endl;
    cout << "Using ProduceString for: I, 1 returned: " << method->produceString("I",1) <<  endl;
*/
    cout << endl << "TEST PRODUCTION METHOD END" << endl << endl;
    return true;
}
#endif // TESTPRODUCTIONMETHOD_H
