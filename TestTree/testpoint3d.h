#ifndef TESTPOINT3D_H
#define TESTPOINT3D_H

#include "../TreeMaker/point3d.h"
#include <iostream>

using namespace std;

bool testPoint3D(){

    Point3D point3D(1,3,6);
    Point3D point3D_second = point3D;
    Point3D default_point3D;
    if(point3D.x != 1 || point3D.y != 3 || point3D.z !=6)
    {
        cout << "Error in Point3D constructor!" << endl;
        return false;
    }
    if(point3D.x != point3D_second.x || point3D.y != point3D_second.y || point3D.z !=point3D_second.z)
    {
        cout << "Error while copying Point3D" << endl;
        return false;
    }
    if(default_point3D.x != 0 || default_point3D.y != 0 || default_point3D.z !=0)
    {
        cout << "Error in default constructor" << endl;
        return false;
    }
    return true;
}


#endif // TESTPOINT3D_H
