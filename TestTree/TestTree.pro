QT       += core
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
TEMPLATE = app
CONFIG += console c++11

SOURCES += main.cpp

HEADERS += \
    testtreenode.h \
    testproductionmethod.h \
    testtreegraph.h \
    testlalphabet.h \
    testpoint3d.h \
    testtreeobject.h

