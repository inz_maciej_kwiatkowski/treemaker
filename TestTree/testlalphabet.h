#ifndef TESTLALPHABET_H
#define TESTLALPHABET_H

#include "../TreeMaker/lalphabet.h"
#include "../TreeMaker/lalphabet.cpp"
#include <iostream>

using namespace std;

bool testLAlphabet(){

    cout << endl << "TEST L_ALPHABET START" << endl << endl;
    LAlphabet alphabet;

    cout << alphabet.getCharFromKey(LKey::NOT_VALID) << endl;
    cout << alphabet.getCharFromKey(LKey::INCREASED_LENGHT) << endl;
    cout << alphabet.getCharFromKey(LKey::NEW_BRANCH) << endl;
    cout << alphabet.getCharFromKey(LKey::OLD_BRANCH) << endl;
    cout << alphabet.getCharFromKey(LKey::NO_OF_PARAMETERS) << endl;

    alphabet.addNewSymbol('B',LKey::OLD_BRANCH);
    alphabet.addNewSymbol('L',LKey::NEW_BRANCH);
    alphabet.addNewSymbol('g',LKey::INCREASED_LENGHT);

    cout << "." << alphabet.getCharFromKey(LKey::NOT_VALID) << "." << endl;
    cout << alphabet.getCharFromKey(LKey::INCREASED_LENGHT) << endl;
    cout << alphabet.getCharFromKey(LKey::NEW_BRANCH) << endl;
    cout << alphabet.getCharFromKey(LKey::OLD_BRANCH) << endl;
    cout << alphabet.getCharFromKey(LKey::NO_OF_PARAMETERS) << endl;
    if(alphabet.getCharFromKey(LKey::OLD_BRANCH)!='B')
        return false;
    if(alphabet.getCharFromKey(LKey::NEW_BRANCH)!='L')
        return false;
    if(alphabet.getCharFromKey(LKey::INCREASED_LENGHT)!='g')
        return false;
    if(alphabet.getCharFromKey(LKey::NO_OF_PARAMETERS)=='g')
        return false;
    if(alphabet.getCharFromKey(LKey::NOT_VALID)=='g')
        return false;
    if(alphabet.getKeyFromChar('B')!=LKey::OLD_BRANCH)
        return false;
    if(alphabet.getKeyFromChar('L')!=LKey::NEW_BRANCH)
        return false;
    if(alphabet.getKeyFromChar('g')!=LKey::INCREASED_LENGHT)
        return false;
    if(alphabet.getKeyFromChar('c')!=LKey::NOT_VALID)
        return false;

    cout << endl << "TEST L_ALPHABET END" << endl << endl;
    return true;
}

#endif // TESTLALPHABET_H
