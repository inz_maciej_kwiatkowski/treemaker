#ifndef TESTTREEOBJECT_H
#define TESTTREEOBJECT_H

#include "../TreeMaker/treeobject.h"
#include <iostream>

using namespace std;

bool testTreeObject(){

    TreeObject object;
    if(object.indices != 0 || object.numberOfIndices != 0 || object.numberOfVertices !=0 || object.vertices !=0)
    {
        cout << "Error in default constructor" << endl;
        return false;
    }
    TreeObject secondObject(9,3);
    if(secondObject.getNumberOfIndices() != 3 || secondObject.getNumberOfVertices() != 9)
    {
        cout << "Error in contructor!" << endl;
        return false;
    }

    return true;
}

#endif // TESTTREEOBJECT_H
