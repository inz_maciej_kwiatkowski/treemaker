#include "testlalphabet.h"
#include "testproductionmethod.h"
#include "testtreegraph.h"
#include "testtreenode.h"
#include "testpoint3d.h"
#include "testtreeobject.h"
#include <assert.h>
#include <QDebug>
#include <iostream>

using namespace std;

void indexed_break(int index);

int main(int argc, char *argv[])
{
    if(testTreeObject())
    {
        cout << "Tests for TreeObject OK!" << endl;
    }
    else
    {
        cout << "TreeObject: Error in the code!";
        return 1;
    }
    if(testPoint3D())
    {
        cout << "Tests for Point3D OK!" << endl;
    }
    else
    {
        cout << "Point3D: Error in the code!";
        return 1;
    }
    if(testLAlphabet())
    {
        cout << "Tests for LAlphabet OK!" << endl;
    }
    else
    {
        cout << "LAlphabet: Error in the code!";
        return 1;
    }
    if(testProductionMethod())
    {
        cout << "Tests for Production Method OK!" << endl;
    }
    else
    {
        cout << "Production Method: Error in the code!";
        return 1;
    }
    if(testTreeNode())
    {
        cout << "Tests for Tree Node OK!" << endl;
    }
    else
    {
        cout << "Test Node: Error in the code!";
        return 1;
    }
/*    if(testTreeGraph())
    {
        cout << "Tests for Tree Graph OK!" << endl;
    }
    else
    {
        cout << "Tree Graph: Error in the code!";
        return 1;
    }*/

    cout << endl << "WSZYSTKIE TESTY PRZESZŁY POMYŚLNIE!" << endl << endl;
    return 0;
}

void indexed_break(int index)
{
    cout << endl;
    cout << index << ".";
    for(int i=0; i< 20; i++) cout << "#";
    cout << endl << endl;
}
