#ifndef LALPHABET_H
#define LALPHABET_H
#include <map>

enum class LKey {
    NOT_VALID = 0,
    NEW_BRANCH, //1
    OLD_BRANCH, //2
    INCREASED_LENGHT, //3

    NO_OF_PARAMETERS //this one should allways be at end

};
LKey& operator++(LKey& k);



class LAlphabet
{

private:
    std::map<LKey, char> symbols;
public:
    LAlphabet();
    void addNewSymbol(char letter, LKey lkey);
    LKey getKeyFromChar(char letter);
    char getCharFromKey(LKey lkey);
};

#endif // LALPHABET_H
