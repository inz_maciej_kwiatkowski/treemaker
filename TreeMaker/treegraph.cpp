#include "treegraph.h"
#include "binarysearchtree.h"
#include <QDebug>
#include <vector>

#define TG "TreeGraph: "

TreeGraph::TreeGraph()
{
    root = NULL;
    numberOfAllEdges = 0;
    numberOfAllVertices = 0;
    numberOfAllNodes = 0;
    treeWidth = 1;
}

TreeGraph::~TreeGraph()
{
    if(root!=NULL)
        delete root;
}

void TreeGraph::addNode()
{/*
    if(root==NULL)
    {
        printf("root is null. Create NEW TreeNode with axiom %s\n",ProductionMethod::getInstance()->getAxiom().c_str());

        root = new TreeNode(0,ProductionMethod::getInstance()->getAxiom());
    }
    else
    {
        printf("root is not null. Add Node\n");
        root->addNode();
    }*/
}

TreeNode *TreeGraph::getRoot()
{
    return root;
}

std::string TreeGraph::printTree()
{
    printf("printing...\n");
    int i = 1;
    std::string out = "1. ";
    TreeNode* node;
    if(root)
        out += root->getProduct();
        out += "\n";
    while(node = root->getNextNode())
    {
        printf("printing %s...\n",node->getProduct());
        char buffer[32];
        out += itoa(++i,buffer,10);
        out += ". ";
        out += node->getProduct();
        out += "\n";
    }
    return out;
}

void TreeGraph::breadthFirst()
{
    Queue<TreeNode*> queue;
    TreeNode *node = root;
    if(node != 0){
        queue.enqueue(node);
        while(!queue.empty()){
            node = queue.dequeue();
            //visit(node);
            if(node->next_left != 0)
                queue.enqueue(node->next_left);
            if(node->next_right != 0)
                queue.enqueue(node->next_right);
        }
    }
}

void TreeGraph::fillTreeObject(TreeObject &object)
{
    std::vector<int> indices;
    std::vector<float> vertices;
    Queue<TreeNode*> queue;
    TreeNode *node = root;
    if(node == 0) return;
    queue.enqueue(node);
    TreeNode* parentNode = NULL;
    int id = node->getId();
    while(!queue.empty())
    {
        node = queue.dequeue();
        qDebug() << TG << "id: " << id;
        for(int i = 0;i<NO_OF_VERTICES_IN_CIRCLE;i++)
        {
            //object.vertices[i+(NO_OF_VERTICES_IN_NODE*id)] = node->points[i].x;
            vertices.push_back(node->points[i].x);
            vertices.push_back(node->points[i].y);
            vertices.push_back(node->points[i].z);
        }
        //object.vertices[NO_OF_VERTICES_IN_CIRCLE+(NO_OF_VERTICES_IN_NODE*id)] = node->top;
        vertices.push_back(node->getTop().x);
        vertices.push_back(node->getTop().y);
        vertices.push_back(node->getTop().z);
        //build walls
        if(parentNode = node->getParentNode())
        {
            for(int i = 1;i<NO_OF_VERTICES_IN_CIRCLE;i++)
            {
                indices.push_back(i+(NO_OF_VERTICES_IN_NODE*id));
                indices.push_back((i+1)+(NO_OF_VERTICES_IN_NODE*id));
                indices.push_back(NO_OF_VERTICES_IN_NODE+(NO_OF_VERTICES_IN_NODE*parentNode->getId()));

                indices.push_back(i+(NO_OF_VERTICES_IN_NODE*parentNode->getId()));
                indices.push_back((i+1)+(NO_OF_VERTICES_IN_NODE*parentNode->getId()));
                indices.push_back(NO_OF_VERTICES_IN_NODE+(NO_OF_VERTICES_IN_NODE*id));
            }
            //last indice
            {
                indices.push_back(NO_OF_VERTICES_IN_CIRCLE+(NO_OF_VERTICES_IN_NODE*id));
                indices.push_back(1+(NO_OF_VERTICES_IN_NODE*id));
                indices.push_back(NO_OF_VERTICES_IN_NODE+(NO_OF_VERTICES_IN_NODE*parentNode->getId()));

                indices.push_back(NO_OF_VERTICES_IN_CIRCLE+(NO_OF_VERTICES_IN_NODE*parentNode->getId()));
                indices.push_back(1+(NO_OF_VERTICES_IN_NODE*parentNode->getId()));
                indices.push_back(NO_OF_VERTICES_IN_NODE+(NO_OF_VERTICES_IN_NODE*id));
            }
        }
        //indices Top
        for(int i = 1;i<NO_OF_VERTICES_IN_CIRCLE;i++)
        {
            int ind1 = i+(NO_OF_VERTICES_IN_NODE*id);
            indices.push_back(ind1);
            int ind2 = (i+1)+(NO_OF_VERTICES_IN_NODE*id);
            indices.push_back(ind2);
            int ind3 = NO_OF_VERTICES_IN_NODE+(NO_OF_VERTICES_IN_NODE*id);
            indices.push_back(ind3);
            qDebug()<< TG << "Ind: " << ind1 << " " << ind2 << " " << ind3 << ", i: " << i << "(NO_OF_VERTICES_IN_NODE*id) " << (NO_OF_VERTICES_IN_NODE*id) << ", id: " << id;
        }
        //last indice
        {
            indices.push_back(NO_OF_VERTICES_IN_CIRCLE+(NO_OF_VERTICES_IN_NODE*id));
            indices.push_back(1+(NO_OF_VERTICES_IN_NODE*id));
            indices.push_back(NO_OF_VERTICES_IN_NODE+(NO_OF_VERTICES_IN_NODE*id));
        }
        if(node->next_left != 0)
            queue.enqueue(node->next_left);
        if(node->next_right != 0)
            queue.enqueue(node->next_right);
    }
    qDebug() << TG << "Vertices: ";
    for(auto v = vertices.begin(); v != vertices.end(); v++)
    {
        qDebug() << TG << "x " << *v;
        v++;
        qDebug() << TG << "y " << *v;
        v++;
        qDebug() << TG << "z " << *v;
    }
    qDebug() << TG << "Indices: ";
    for(auto v = indices.begin(); v != indices.end(); v++)
    {
        qDebug() << TG  << *v;
    }
    object.createIndices(indices.size());
    object.createVerticesContainer(vertices.size());
    object.setIndices(indices);
    object.setVertices(vertices);
}

void TreeGraph::increaseTree()
{
    if(!root)
    {
        qDebug() << TG << "Creating root" ;
        Point3D point;
        qDebug() << TG << "create axiom" ;
        char axiom[2];
        qDebug() << TG << "get axiom" ;
        axiom[0] = ProductionMethod::getInstance()->getAxiom();
        qDebug() << TG << "set second char to \'\0\'" ;
        axiom[1] = '\0';
        qDebug() << TG << axiom;
        qDebug() << TG << "new Root" ;
        root = new TreeNode(0,axiom,treeWidth,point,numberOfAllNodes++);
        qDebug() << TG << "increase number of nodes" ;
        numberOfAllNodes++;
    }
    preorder(root);
}

void TreeGraph::preorder(TreeNode *node)
{

    Queue<TreeNode*> queue;
    TreeNode *tn = node;
    if(tn != 0){
        queue.enqueue(tn);
        qDebug() << TG << "enqueue node.";
        while(!queue.empty()){
            tn = queue.dequeue();
            qDebug() << TG << "Produce";
            tn->produce();
            if(tn->next_left != 0)
                queue.enqueue(tn->next_left);
            if(tn->next_right != 0)
                queue.enqueue(tn->next_right);
        }
    }
}


