#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug> // do usunięcia
#include <QGLFormat>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    QGLFormat glFormat;
     glFormat.setVersion( 3, 3 );
     glFormat.setProfile( QGLFormat::CoreProfile ); // Requires >=Qt-4.8.0
     glFormat.setSampleBuffers( true );
    ui->setupUi(this);
    connect(ui->quiteButton,SIGNAL(clicked(bool)),this,SLOT(close()));
    connect(ui->generateButton,SIGNAL(clicked(bool)),this,SLOT(generate()));
    connect(ui->eksportButton,SIGNAL(clicked(bool)),this,SLOT(eksport()));
    connect(ui->useRuleButton,SIGNAL(clicked(bool)),this,SLOT(useRule()));
    connect(ui->changeRuleButton,SIGNAL(clicked(bool)),this,SLOT(changeRule()));

    lalphabet.addNewSymbol('L',LKey::NEW_BRANCH);
    lalphabet.addNewSymbol('B',LKey::OLD_BRANCH);
    lalphabet.addNewSymbol('g',LKey::INCREASED_LENGHT);

    ProductionMethod::init('L',lalphabet);
    lSystem = ProductionMethod::getInstance();

    useRule();
    treeGraph = nullptr;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::generate()
{
    qDebug() << "GENERATE" ;
    if(treeGraph == nullptr)
        delete treeGraph;
    treeGraph = new TreeGraph;
    int iterations = ui->iterationSpin->value();
    qDebug() << "Genereting tree for" << iterations << "iterations";
    for(int i=0; i<iterations; i++)
    {
        treeGraph->increaseTree();
    }
    TreeObject object;
    treeGraph->fillTreeObject(object);
    ui->widget->sendForPainting(object.vertices, object.numberOfVertices,object.indices,object.numberOfIndices);
    //ui->widget->draw(object.vertices, object.numberOfVertices,object.indices);
}

void MainWindow::eksport()
{
    qDebug() << "EKSPORT" ;
    QString fileName = QFileDialog::getSaveFileName
                    (this,tr("Zapisz plik jako..."),tr("/home/"),tr("Pliki tekstowe (*.obj)"));

    if(fileName.isEmpty())
        return;

    QFile plik(fileName);
    plik.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text);

    QTextStream stream(&plik);

    if(treeGraph == nullptr)
        delete treeGraph;
    treeGraph = new TreeGraph;
    int iterations = ui->iterationSpin->value();
    for(int i=0; i<iterations; i++)
    {
        treeGraph->increaseTree();
    }
    TreeObject object;
    treeGraph->fillTreeObject(object);
    stream << "#Tree generated in TreeMaker with L-System\n";
    for(int i=0; i<object.numberOfVertices;)
    {
        stream << "\nv ";
        for(int j=0; j<3; j++,i++)
            stream << object.vertices[i] << " ";
    }
    for(int i=0; i<object.numberOfIndices;)
    {
        stream << "\nf ";
        for(int j=0; j<3; j++,i++)
            stream << object.indices[i] << " ";
    }

    //poprzedniaSciezka = fileName;

    plik.close();
}

void MainWindow::changeRule()
{
    qDebug() << "CHANGE_RULE";
}

void MainWindow::useRule()
{
    qDebug() << "USE_RULE";
    QString axiom = ui->axiomEdit->text();
    qDebug() << "init LSystem with axiom \'" << axiom[0].toLatin1() << "\'";
    lSystem->init(axiom[0].toLatin1(),lalphabet);
    QString textB = ui->BEdit->text();
    qDebug() << "addProductionString with key " << ui->BLabel->text().at(0).toLatin1() << " and text " << textB.toUtf8().constData();
    lSystem->addProductionString(ui->BLabel->text().at(0).toLatin1(),textB.toUtf8().constData());
    QString textL = ui->LEdit->text();
    qDebug() << "addProductionString with key " << ui->LLabel->text().at(0).toLatin1() << " and text " << textL.toUtf8().constData();
    lSystem->addProductionString(ui->LLabel->text().at(0).toLatin1(),textL.toUtf8().constData());
    QString textG = ui->gEdit->text();
    qDebug() << "addProductionString with key " << ui->gLabel->text().at(0).toLatin1() << " and text " << textG.toUtf8().constData();
    lSystem->addProductionString(ui->gLabel->text().at(0).toLatin1(),textG.toUtf8().constData());
}

bool MainWindow::validateRule(QString text)
{
    return false;
}
