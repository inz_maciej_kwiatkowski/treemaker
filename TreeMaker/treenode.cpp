#include "treenode.h"
#include <cmath>
#include <qdebug.h>

#define TN "TreeNode: "

int TreeNode::getId()
{
    return id;
}

Point3D TreeNode::getTop()
{
    return top;
}

TreeNode::TreeNode(TreeNode* parent)
{
    printf("Creating Node");
    prev = parent;
    next_left = NULL;
    next_right = NULL;
    lastReturnedNode = NULL;
    isLeaf = true;
    id = 0;
}

TreeNode::TreeNode(TreeNode *parent, char* _product)
{
    printf("Creating Node with axiom: %s",_product);
    prev = parent;
    next_left = NULL;
    next_right = NULL;
    lastReturnedNode = NULL;
    strcpy(this->product,_product);
    //this->product = _product;
    isLeaf = true;
    id = 0;
}

TreeNode::TreeNode(TreeNode *parent, char* _product, float circleWidth, Point3D middle, int _id)
{
    printf("Creating Node with axiom: %s",_product);
    qDebug() << TN << _product;
    prev = parent;
    next_left = NULL;
    next_right = NULL;
    lastReturnedNode = NULL;
    qDebug()<< TN  << "strcpy";
    strcpy(this->product,_product);
    int i = 0;
    for(;_product[i] != '\0';i++);
    memcpy(product,_product,i);
    qDebug() << TN << "produce circle";
    /*produceCircle(circleWidth, middle);
    qDebug() << TN << "circle ready";
    top = middle;
    top.y = middle.y+0.2f;
    isLeaf = true;
    id = _id;
    */
}

void TreeNode::produceCircle(float circleWidth, Point3D middle)
{
    const float DEG2RAD = 3.14159f/180;
    float radius = circleWidth/2;
    int step = 360/NO_OF_VERTICES_IN_CIRCLE;
    int pointsIndex = 0;
    for (int i=0; i < 360; i+=step)
    {
      float degInRad = i*DEG2RAD;
      float x = cos(degInRad)*radius;
      float z = sin(degInRad)*radius;
      Point3D point(x+middle.x, middle.y, z+middle.z);
      points[pointsIndex++] = point;
    }
}

void TreeNode::addNextLeft(TreeNode *next)
{
    next_left = new TreeNode(next);
}

void TreeNode::addNextRight(TreeNode *next)
{
    next_right = next;
}

void TreeNode::addNextLeft(TreeNode *next, char* product)
{
    next_left = new TreeNode(next,product);
}

void TreeNode::addNextRight(TreeNode *next, char *product)
{
    next_right = new TreeNode(next,product);
}

branchesE TreeNode::getBranches()
{
    return branches;
}

int TreeNode::getNodeNumber()
{
    return nodeNumber;
}

void TreeNode::addNode()
{
    if(next_left == NULL)
    {
        addNextLeft(this);
    }
    else if(next_right == NULL)
    {
        addNextRight(this);
    }
}

void TreeNode::addNode(char* product)
{
    if(next_left == NULL)
    {
        addNextLeft(this, product);
    }
    else if(next_right == NULL)
    {
        addNextRight(this, product);
    }
}

char *TreeNode::getProduct()
{
    return this->product;
}

void TreeNode::produce()
{
    qDebug() << TN << "PRODUCING" << this->product;
    Production prod;
    prod = ProductionMethod::getInstance()->produce(this->product);
    qDebug() << TN << "B: " << prod.numOfOldBranches << " L: " << prod.numOfNewBranches << " g: " << prod.increaseLentgh;
    if(prod.increaseLentgh > 0 )
        qDebug() << TN << "produce: increase lenght";
    if(prod.numOfOldBranches > 0)
        qDebug() << __FILE__ << ", should create new nodes";
    if(prod.numOfOldBranches > 0)
        qDebug() << __FILE__ << __LINE__ << ", should be old branch";
}

TreeNode *TreeNode::getNextNode()
{
    if(lastReturnedNode == NULL)
    {
        if(next_left != NULL )
        {
            lastReturnedNode = next_left;
        }
    }
    else if(lastReturnedNode == next_left)
    {
        if(next_right != NULL)
        {
            lastReturnedNode = next_right;
        }
        else
        {
            lastReturnedNode = NULL;
        }
    }
    return lastReturnedNode;
}

TreeNode *TreeNode::getParentNode()
{
    return prev;
}

TreeNode*& TreeNode::getLeftBranch()
{
    return next_left;
}

TreeNode*& TreeNode::getRightBranch()
{
    return next_right;
}
