#ifndef TREEOBJECT_H
#define TREEOBJECT_H

#include <vector>

class TreeObject{
public:
    float* vertices;
    int numberOfVertices;
    int* indices;
    int numberOfIndices;

public:
    TreeObject():vertices(0),numberOfVertices(0),indices(0),numberOfIndices(0)
    {
    }

    TreeObject(int verticesNumber, int indicesNumber)
    {
        numberOfVertices = verticesNumber;
        numberOfIndices = indicesNumber;
        vertices = new float[numberOfVertices];
        indices = new int[numberOfIndices];
    }

    void createVerticesContainer(int verticesNumber)
    {
        if(numberOfVertices)
            delete [] vertices;
        numberOfVertices = verticesNumber;
        vertices = new float[numberOfVertices];
    }

    void createIndices(int indicesNumber)
    {
        if(numberOfIndices)
            delete [] indices;
        numberOfIndices = indicesNumber;
        indices = new int[numberOfIndices];
    }


    void setVertices(float vert[], int numOfVert)
    {
        if(numOfVert > numberOfVertices)
        {
            return;
        }
        for(int i=0; i < numOfVert; i++)
        {
            vertices[i] = vert[i];
        }
    }
    void setVertices(std::vector<float> vec)
    {
        if(vec.size() > numberOfVertices)
        {
            return;
        }
        for(int i=0; i < vec.size(); i++)
        {
            vertices[i] = vec[i];
        }
    }

    void setVerticesFromSpecIndex(float vert[], int numOfVert, int startIndex)
    {
        if(numOfVert+startIndex > numberOfVertices)
        {
            return;
        }
        int vertIndex = startIndex;
        int incomeVertIndex = 0;
        while(incomeVertIndex < numOfVert)
        {
            vertices[vertIndex] = vert[incomeVertIndex];
            vertIndex++;
            incomeVertIndex++;
        }
    }

    void setIndices(int ind[], int numOfInd)
    {
        if(numOfInd > numberOfIndices)
        {
            return;
        }
        for(int i=0; i < numOfInd; i++)
        {
            indices[i] = ind[i];
        }
    }

    void setIndices(std::vector<int> ind)
    {
        if(ind.size() > numberOfIndices)
        {
            return;
        }
        for(int i=0; i < ind.size(); i++)
        {
            indices[i] = ind[i];
        }
    }

    void setIndicesFromSpecIndex(int ind[], int numOfInd, int startIndex)
    {
        if(numOfInd+startIndex > numberOfIndices)
        {
            return;
        }
        int indIndex = startIndex;
        int incomeIndIndex = 0;
        while(incomeIndIndex < numOfInd)
        {
            indices[indIndex] = ind[incomeIndIndex];
            indIndex++;
            incomeIndIndex++;
        }
    }

    int getNumberOfVertices()
    {
        return numberOfVertices;
    }

    int getNumberOfIndices()
    {
        return numberOfIndices;
    }

    ~TreeObject()
    {
        if(numberOfVertices)
            delete [] vertices;
        if(numberOfIndices)
            delete [] indices;
    }
};

#endif // TREEOBJECT_H
