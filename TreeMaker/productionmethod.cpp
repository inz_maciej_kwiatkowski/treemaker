#include "productionmethod.h"
#include <QDebug>

#define PM "ProductionMethod: "

ProductionMethod* ProductionMethod::instance;
ProductionMethod::ProductionMethod(char axiom, LAlphabet alphabet)
{
    printf("/\\/\\/\\Creating Instance of ProdutionMethod!/\\/\\/\\");
    this->axiom = axiom;
    this->_alphabet = alphabet;
}

void ProductionMethod::init(char axiom, LAlphabet alphabet)
{
    instance = new ProductionMethod(axiom, alphabet);
}


void ProductionMethod::addProductionString(std::string productionStr)
{
    std::size_t pos = productionStr.find("->");
    printf("Tree: pos %u\n",pos);
    char axiom = productionStr.substr(0,pos)[0];
    printf("Tree: axiom %c\n",axiom);
    std::string product = productionStr.substr(pos+2);
    printf("Tree: product %s\n",product.c_str());
    instance->productionMap[axiom] = product;// = product;
}

void ProductionMethod::addProductionString(char key, std::string productionStr)
{
    instance->productionMap[key] = productionStr;
}

bool ProductionMethod::produce(char axiom, std::string& product)
{
    auto it = instance->productionMap.find(axiom);
    if(it != instance->productionMap.end())
    {
        product = it->second;
        return true;
    }
    product = "";
    return false;
}


Production ProductionMethod::produce(char axiom)
{
    Production prod;
    auto it = instance->productionMap.find(axiom);
    if(it != instance->productionMap.end())
    {
        std::string productString = it->second;
        for(int i = 0; i<productString.length(); i++)
        {
            prod.productFromLKey(_alphabet.getKeyFromChar((char)productString[i]));
        }
    }
    printf("produce(): produced %d new, %d old, %d increaseLength",prod.numOfNewBranches,prod.numOfOldBranches,prod.increaseLentgh);
    return prod;
}

Production ProductionMethod::produce(const char *axiom)
{
    Production prod;
    for(int a = 0; axiom[a] != '\0'; a++)
    {

        auto it = instance->productionMap.find(axiom[a]);
        if(it != instance->productionMap.end())
        {
            std::string productString = it->second;
            for(int i = 0; i<productString.length(); i++)
            {
                prod.productFromLKey(_alphabet.getKeyFromChar((char)productString[i]));
            }
        }
        printf("produce(): produced %d new, %d old, %d increaseLength",prod.numOfNewBranches,prod.numOfOldBranches,prod.increaseLentgh);
    }
    return prod;
}
/*
std::string ProductionMethod::produceString(std::string axiom, int branchIndex)
{
    int product = (int)ProductE::NOT_VALID;
    auto it = instance->productionMap.find(axiom);
    if(it != instance->productionMap.end())
    {
        if(branchIndex == -1)
            return it->second;
        else
        {
            return devideString(it->second,branchIndex);
        }

    }
    return "product";
}
*/
std::string ProductionMethod::devideString(std::string str, int stringPart)
{
    std::size_t posX = str.find("X");
    std::size_t posY = str.find("Y");
    std::size_t posI = str.find("I");
    qDebug() << PM << "posX: " << posX << ", posY: " << posY << ", posI: " << posI;
    return "product";
}

std::string ProductionMethod::printAlphabet()
{
    return "Nic tu nie ma";
}

std::string ProductionMethod::printProductionRules()
{
    return "Nic tu nie ma";
}


ProductionMethod* ProductionMethod::getInstance()
{
    if(!instance)
    {
        printf("Production method should be initialized first!\nReturn NULL.\n");
        return NULL;
    }
    return instance;
}

ProductionMethod::~ProductionMethod()
{}


char ProductionMethod::getAxiom()
{
    printf("Returning axiom: %c",axiom);
    return instance->axiom;
}
