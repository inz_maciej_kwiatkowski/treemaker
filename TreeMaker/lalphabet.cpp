#include "lalphabet.h"

LKey& operator++(LKey& k){
    switch(k){
    case(LKey::NEW_BRANCH):
        return k=LKey::OLD_BRANCH;
    case(LKey::OLD_BRANCH):
        return k=LKey::INCREASED_LENGHT;
    case(LKey::INCREASED_LENGHT):
        return k=LKey::NO_OF_PARAMETERS;
    default:
        return k=LKey::NOT_VALID;
    }
    return k=LKey::NOT_VALID;
}

LAlphabet::LAlphabet()
{
}

void LAlphabet::addNewSymbol(char letter, LKey lkey)
{
    symbols[lkey] = letter;
}

LKey LAlphabet::getKeyFromChar(char letter)
{
    for(LKey i = LKey::NEW_BRANCH; i < LKey::NO_OF_PARAMETERS; ++i)
    {
        if(letter == symbols[i])
            return i;
    }
    return LKey::NOT_VALID;
}

char LAlphabet::getCharFromKey(LKey lkey)
{
    return symbols[lkey];
}
