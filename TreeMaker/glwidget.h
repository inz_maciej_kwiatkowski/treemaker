#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QOpenGLFunctions_3_3_Core>
#include <QGLWidget>
#include <QMouseEvent>

#include <QGLBuffer>
#include <QGLShaderProgram>

//Used to transform vectors
//#include <glm/glm.hpp>
//#include <glm/gtc/matrix_transform.hpp>
//#include <glm/gtc/type_ptr.hpp>


class GLWidget : public QGLWidget, protected QOpenGLFunctions_3_3_Core
{
    Q_OBJECT
public:
    explicit GLWidget(QGLFormat& format, QWidget *parent = 0);
    explicit GLWidget(QWidget *parent = 0);
    void initializeGL();
    void paintGL();
    void resizeGL(int w, int h);
    void drawSurface();
    void drawDirections();
    void drawTree();
    void drawCylinder(float bottom_spot, float top_spot, float bottom_radius, float top_radius, float height);
    void draw(GLfloat vertices[], GLsizeiptr sizeof_vertices, GLuint indices[]);
    void sendForPainting(float* vertices, int sizeof_vertices, int* indices, int sizeof_indices);
    void draw(GLfloat vertices[], GLsizeiptr sizeof_vertices, GLuint indices[], GLfloat normals[]);
    void draw(GLfloat vertices[], GLsizeiptr sizeof_vertices, GLuint indices[], GLfloat normals[], GLfloat colors[]);


protected slots:
    void setXRotation(int angle);
    void setYRotation(int angle);
    void setZRotation(int angle);

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void wheelEvent(QWheelEvent* event);
    //void initCamera();
    GLfloat screenWidth = 800.0;
    GLfloat screenHeight = 600.0;

private:
    int xRot;
    int yRot;
    int zRot;
    QPoint lastPos;
    GLfloat* treeVert;
    GLuint* treeInd;
    GLsizeiptr treeSize;
    bool sendingTree;

private:
    bool prepareShaderProgram();

    QOpenGLFunctions_4_3_CoreBackend* m_funcs;
    QOpenGLContext *m_context;
    //QGLShaderProgram m_shader;
   // QGLBuffer m_vertexBuffer;
};

#endif // GLWIDGET_H
