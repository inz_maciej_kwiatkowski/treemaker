#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "treegraph.h"
#include "productionmethod.h"
#include "lalphabet.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void generate();
    void eksport();
    void changeRule();
    void useRule();

private:
    Ui::MainWindow *ui;
    bool validateRule(QString text);
    ProductionMethod *lSystem;
    LAlphabet lalphabet;
    TreeGraph *treeGraph;
};

#endif // MAINWINDOW_H
