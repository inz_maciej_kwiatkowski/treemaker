#ifndef TREEGRAPH_H
#define TREEGRAPH_H
#include "treenode.h"
#include "productionmethod.h"
#include "treeobject.h"

class TreeGraph
{
protected:
    TreeNode* root;
    int numberOfAllVertices;
    int numberOfAllEdges;
    int numberOfAllNodes;
    float treeWidth;
public:
    TreeGraph();
    ~TreeGraph();
    void addNode();
    TreeNode* getRoot();
    std::string printTree();
    void breadthFirst();
    void fillTreeObject(TreeObject& object);
    void increaseTree();
private:
    void preorder(TreeNode* node);
};

#endif // TREEGRAPH_H
