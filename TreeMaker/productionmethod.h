#ifndef PRODUCTIONMETHOD_H
#define PRODUCTIONMETHOD_H
#include <string>
#include <map>
#include "lalphabet.h"
//#include "treenode.h"

struct Production{
    int numOfNewBranches = 0;
    int increaseLentgh = 0;
    int numOfOldBranches = 0;
    void productFromLKey(LKey lkey)
    {
        printf("Received LKey %d\n",(int)lkey);
        switch(lkey)
        {
        case(LKey::NEW_BRANCH):
            numOfNewBranches++;
            break;
        case(LKey::OLD_BRANCH):
            numOfOldBranches++;
            break;
        case(LKey::INCREASED_LENGHT):
            increaseLentgh++;
            break;
        default:
            printf("Wrong LKey\n");
        }
    }
};

class ProductionMethod
{
protected:
    std::map<char, std::string> productionMap;
    char axiom = ' ';
private:
    ProductionMethod(char axiom, LAlphabet alphabet);
    static ProductionMethod* instance;
    LAlphabet _alphabet;
public:
    static void init(char axiom, LAlphabet alphabet);
    void addProductionString(std::string productionStr);
    void addProductionString(char key, std::string productionStr);
    bool produce(char axiom, std::string& product);
    //ta metoda nie jest najlepsza. Nie da się np. zwrócić 2x new_branch + old_branch
    //update: dołożony nowy enum określający 2 nowe gałęzie 'LL'
    Production produce(char axiom);
    Production produce(const char* axiom);
    std::string printAlphabet();
    std::string printProductionRules();
    static ProductionMethod *getInstance();
    ~ProductionMethod();
    //std::string produceNextNode(TreeNode parentNode);
    //bool generateNode(TreeNode& parentNode);
    char getAxiom();

    // branchIndex służy określeniu dla której branchy chcemy wyprodukować łańcuch znaków
    // domyślne -1 zwraca całe słowo
    // wartość 0 zwraca pierwszą wartość w indeksie itd..
//    std::string produceString(std::string axiom, int branchIndex = -1);
    std::string devideString(std::string str, int stringPart);
};

#endif // PRODUCTIONMETHOD_H
