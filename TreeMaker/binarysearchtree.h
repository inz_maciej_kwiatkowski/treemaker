#ifndef BINARYSEARCHTREE_H
#define BINARYSEARCHTREE_H

#include <queue>
#include <stack>

using namespace std;

template<class T>
class Queue: public queue<T>{
public:
    T dequeue(){
        T tmp = front();
        queue<T>::pop();
        return tmp;
    }

    void enqueue(const T& element){
        push(element);
    }
};
/*
template<class T>
class BSTNode{
public:
    BSTNode(){
        left = right = 0;
    }
    BSTNode(const T& element, BSTNode *l = 0, BSTNode *l = 0){
        key = element;
        left = l;
        right = r;
    }
    T key;
    BSTNode *left;
    BSTNode *right;
};

template<class T>
class BST {
public:
    BST(){
        root = 0;
    }
    ~BST(){
        clear();
    }
    void clear(){
        clear(root);
        root = 0;
    }
    bool isEmpty() const{
        return root==0;
    }
    void preorder(){
        preorder(root);
    }
    void inorder(){
        inorder(root);
    }
    void postorder(){
        postorder(root);
    }
    T* search(const T& el) const{
        return search(root.el);
    }
    void breadthFirst();
    void iterativePreorder();
    void iterativeInorder();
    void iterativePostorder();
    void MorrisInorder();
    void insert(const T&);
    void deleteByMerging(BSTNode<T>*&);
    void findAndDeleteByMerging(const T&);
};

*/
#endif // BINARYSEARCHTREE_H
