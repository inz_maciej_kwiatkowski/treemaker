#ifndef POINT3D_H
#define POINT3D_H

struct Point3D{
    float x;
    float y;
    float z;

    Point3D():x(0),y(0),z(0)
    {}

    Point3D(float _x, float _y, float _z)
    {
        x = _x;
        y = _y;
        z = _z;
    }

    Point3D(const Point3D& point)
    {
        x = point.x;
        y = point.y;
        z = point.z;
    }
};

#endif // POINT3D_H
