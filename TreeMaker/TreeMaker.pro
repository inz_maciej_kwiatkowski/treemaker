#-------------------------------------------------
#
# Project created by QtCreator 2017-01-14T17:42:53
#
#-------------------------------------------------

QT       += core gui opengl

#greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TreeMaker
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    glwidget.cpp \
    treenode.cpp \
    treegraph.cpp \
    productionmethod.cpp \
    productionmethod.cpp \
    treegraph.cpp \
    treenode.cpp \
    lalphabet.cpp

HEADERS  += mainwindow.h \
    glwidget.h \
    treenode.h \
    treegraph.h \
    productionmethod.h \
    lalphabet.h \
    treeobject.h \
    point3d.h \
    binarysearchtree.h

FORMS    += mainwindow.ui

LIBS += -lopengl32 \
        -lglu32

DISTFILES +=
