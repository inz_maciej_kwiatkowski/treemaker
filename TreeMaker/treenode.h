#ifndef TREENODE_H
#define TREENODE_H
#include <string>
#include "point3d.h"
#include "treeobject.h"
#include "productionmethod.h"

#define NO_OF_VERTICES_IN_CIRCLE 8
#define NO_OF_VERTICES_IN_NODE (NO_OF_VERTICES_IN_CIRCLE+1)
#define TREE_WIDTH_AT_START 0.01f
#define TREE_WIDTH_MULTIPLICATION 1.2f
#define TREE_HEIGHT_AT_START 0.1f
#define TREE_HEIGHT_MULTIPLICATION 1.3f

enum branchesE
{
    NotValid = 0,
    SingleBranch = 1,
    TwoBranches  = 2
};

//based on bit tree
class TreeNode
{
protected:
    TreeNode* prev;

    char* product;
    int rotateX;
    int rotateY;
    int rotateZ;
    branchesE branches;
    bool fullyProduced;
    TreeNode* lastReturnedNode;
    int nodeNumber;
    int id;
    Point3D top;
public:
    TreeNode* next_left;
    TreeNode* next_right;
    bool isLeaf;
    int getId();
    Point3D getTop();
    Point3D points[NO_OF_VERTICES_IN_CIRCLE];
    TreeNode(TreeNode* parent);
    TreeNode(TreeNode* parent, char* _product);
    TreeNode(TreeNode* parent, char* _product, float circleWidth, Point3D middle, int _id);
    void produceCircle(float circleWidth, Point3D middle);
    void addNextLeft(TreeNode* next);
    void addNextRight(TreeNode* next);
    void addNode();
    void addNode(char* product);
    char* getProduct();
    void produce();
    void setBranches(branchesE branches);
    TreeNode* getNextNode();
    TreeNode *getParentNode();
    TreeNode*& getLeftBranch();
    TreeNode*& getRightBranch();
    void addNextLeft(TreeNode *next, char *product);
    void addNextRight(TreeNode *next, char* product);
    branchesE getBranches();
    int getNodeNumber();
};

#endif // TREENODE_H
