#include "glwidget.h"
#include <ctime>
#include "gl/GL.h"
#include "gl/GLU.h"
#include <QDebug>
#include <cmath>
#include <QDateTime>
#define PI 3.1415926535

#define OGL "GLWidget: "



GLWidget::GLWidget(QGLFormat& format, QWidget *parent): QGLWidget(format,parent)//, QOpenGLFunctions_3_3_Core()
{
    xRot = 0;
    yRot = 0;
    zRot = 0;
}

GLWidget::GLWidget(QWidget *parent)
{
    QGLFormat glFormat;
    glFormat.setVersion( 3, 3 );
    glFormat.setProfile( QGLFormat::CoreProfile ); // Requires >=Qt-4.8.0
    glFormat.setSampleBuffers( true );
    treeSize = 0;
    sendingTree = false;
    treeVert = NULL;
    treeInd = NULL;

    QGLWidget(glFormat, parent);
    xRot = 0;
    yRot = 0;
    zRot = 0;

    m_context = new QOpenGLContext;

    if ( !m_funcs ) {
        qWarning("Could not obtain OpenGL versions object" );
        exit( 1 );
    }
}

void GLWidget::initializeGL()
{
    initializeOpenGLFunctions();
    glClearColor(0.2,0.2,0.2,1);
    glEnable(GL_DEPTH_CLAMP);
    //glEnable(GL_DEPTH_TEST);

    char *GL_version=(char *)glGetString(GL_VERSION);
    char *GL_vendor=(char *)glGetString(GL_VENDOR);
    char *GL_renderer=(char *)glGetString(GL_RENDERER);
    qDebug() << OGL << "OpenGl version:\t " << GL_version;
    qDebug() << OGL << "OpenGl vendor:\t " << GL_vendor;
    qDebug() << OGL << "OpenGl renderer:\t " << GL_renderer;

    glDepthRange(10,-10);
    resizeGL(screenWidth,screenHeight);
}

void GLWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //glMatrixMode(GL_PROJECTION);
      glLoadIdentity();

      // choose the appropriate projection based on the currently toggled mode

        // set the perspective with the appropriate aspect ratio


        // set up an orthographic projection with the same near clip plane
        //glOrtho(-1.0, 1.0, -1.0, 1.0, -5, 100);

    glRotatef(xRot / 16.0, 1.0, 0.0, 0.0);
    glRotatef(yRot / 16.0, 0.0, 1.0, 0.0);
    glRotatef(zRot / 16.0, 0.0, 0.0, 1.0);

    drawSurface();

    drawDirections();

    drawTree();


}

void GLWidget::resizeGL(int w, int h)
{
    qDebug() << OGL << "RESIZE";
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    float ratio = w/h;
    glFrustum(-ratio, ratio, -1.0, 1.0, -5, 20);
    glMatrixMode(GL_MODELVIEW);
}

void GLWidget::drawSurface()
{
    glLineWidth(0.2);
    glColor3f(1.0, 1.0, 1.0);
    // Turn on wireframe mode
    glPolygonMode(GL_FRONT, GL_LINE);
    glPolygonMode(GL_BACK, GL_LINE);


    for(int i = 0; i < 20; i++)
    {
        glBegin(GL_LINES);
            glVertex3f(-10.0f, 0.0f, -10.0f+i);
            glVertex3f(10.0f, 0.0f, -10.0f+i);
        glEnd();

        glBegin(GL_LINES);
            glVertex3f(-10.0f+i, 0.0f, -10.0f);
            glVertex3f(-10.0f+i, 0.0f, 10.0f);
        glEnd();
    }

    // Turn off wireframe mode
    glPolygonMode(GL_FRONT, GL_FILL);
    glPolygonMode(GL_BACK, GL_FILL);
}

void GLWidget::drawDirections()
{
    glLineWidth(1.5);
    glColor3f(1.0, 0.0, 0.0);
    glBegin(GL_LINES);
        glVertex3f(0, 0.1, 0);
        glVertex3f(1, 0.1, 0);
    glEnd();

    glColor3f(0.0, 1.0, 0.0);
    glBegin(GL_LINES);
        glVertex3f(0, 0.1, 0);
        glVertex3f(0, 1.1, 0);
    glEnd();

    glColor3f(0.0, 0.0, 1.0);
    glBegin(GL_LINES);
        glVertex3f(0, 0.1, 0);
        glVertex3f(0, 0.1, 1);
    glEnd();
}

void GLWidget::drawTree()
{
    qDebug() << OGL << "drawing..";
    if(sendingTree)
        return;
    if(!treeVert || !treeSize)
        return;
    qDebug() << OGL << "DRAW!!";
    draw(treeVert,treeSize,treeInd);
    qDebug() << OGL << "tree";
}

void GLWidget::drawCylinder(float bottom_spot, float top_spot, float bottom_radius, float top_radius, float height)
{
    glLineWidth(1.5);
    glColor3f(1.0, 0.0, 0.0);
    //float radius = 1, halfLength = height/2;
    int slices = 12;
    for(int i = 0; i<slices; i++)
    {
        qDebug() << OGL << "Produce slices " << i;
        float theta = ((float)i)*2.0*PI;
        float nextTheta = ((float)i+1)*2.0*PI;
        glBegin(GL_TRIANGLE_STRIP);
            /*vertex at middle of end */
            glVertex3f(0, bottom_spot, 0);
            /*vertices at edges of circle*/
            glVertex3f(bottom_radius*cos(theta), bottom_spot, bottom_radius*sin(theta));
            glVertex3f (bottom_radius*cos(nextTheta), bottom_spot, bottom_radius*sin(nextTheta));
            /* the same vertices at the bottom of the cylinder*/
            glVertex3f (top_radius*cos(nextTheta), height, top_radius*sin(nextTheta));
            glVertex3f(top_radius*cos(theta), height, top_radius*sin(theta));
            glVertex3f(0, height, 0);
        glEnd();
    }
}


void GLWidget::draw(GLfloat vertices[], GLsizeiptr sizeof_vertices, GLuint indices[])
{
    // enable and specify pointers to vertex arrays
    // set the perspective with the appropriate aspect ratio
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, vertices);

    glPushMatrix();
    //glTranslatef(0.15, 0.15, 0.15);                // move to bottom-left corner
    //glScalef(0.5f,0.5f,0.5f);

    glDrawElements(GL_TRIANGLES, sizeof_vertices, GL_UNSIGNED_BYTE, indices);

    glPopMatrix();

    glDisableClientState(GL_VERTEX_ARRAY);  // disable vertex arrays
    updateGL();
}

void GLWidget::sendForPainting(float *vertices, int sizeof_vertices, int *indices, int sizeof_indices)
{
    qDebug() << OGL << "sending...";
    sendingTree = true;
    if(treeVert){
        delete [] treeVert;
        treeVert = NULL;
        treeSize = 0;
    }
    treeVert = new GLfloat[sizeof_vertices];
    treeSize = sizeof_vertices;
    for(int i=0; i<sizeof_vertices; i++)
    {
        treeVert[i] = vertices[i];
        qDebug() << OGL << treeVert[i];
    }
    if(treeInd){
        delete [] treeInd;
        treeInd = NULL;
    }
    treeInd = new GLuint[sizeof_indices];
    for(int i=0; i<sizeof_indices; i++)
    {
        treeInd[i] = indices[i];
        qDebug() << treeInd[i];
    }
    sendingTree = false;
    qDebug()<< OGL  << "received...";
    updateGL();
}

void GLWidget::draw(GLfloat vertices[], GLsizeiptr sizeof_vertices, GLuint indices[], GLfloat normals[])
{
    // enable and specify pointers to vertex arrays
    // set the perspective with the appropriate aspect ratio
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_VERTEX_ARRAY);
    glNormalPointer(GL_FLOAT, 0, normals);
    glVertexPointer(3, GL_FLOAT, 0, vertices);

    glPushMatrix();
    //glTranslatef(0.15, 0.15, 0.15);                // move to bottom-left corner
    //glScalef(0.5f,0.5f,0.5f);

    glDrawElements(GL_TRIANGLES, sizeof_vertices, GL_UNSIGNED_INT, indices);

    glPopMatrix();

    glDisableClientState(GL_VERTEX_ARRAY);  // disable vertex arrays
    glDisableClientState(GL_NORMAL_ARRAY);
}

void GLWidget::draw(GLfloat vertices[], GLsizeiptr sizeof_vertices, GLuint indices[], GLfloat normals[], GLfloat colors[])
{

    // enable and specify pointers to vertex arrays
    // set the perspective with the appropriate aspect ratio
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_VERTEX_ARRAY);
    glNormalPointer(GL_FLOAT, 0, normals);
    glColorPointer(3, GL_FLOAT, 0, colors);
    glVertexPointer(3, GL_FLOAT, 0, vertices);

    glPushMatrix();
    //glTranslatef(0.15, 0.15, 0.15);                // move to bottom-left corner
    //glScalef(0.5f,0.5f,0.5f);

    glDrawElements(GL_TRIANGLES, sizeof_vertices, GL_UNSIGNED_BYTE, indices);

    glPopMatrix();

    glDisableClientState(GL_VERTEX_ARRAY);  // disable vertex arrays
    glDisableClientState(GL_COLOR_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
}

void GLWidget::mousePressEvent(QMouseEvent *event)
{
    qDebug() << OGL << "Mouse pressed!";
    lastPos = event->pos();
}

void GLWidget::mouseMoveEvent(QMouseEvent *event)
{
    qDebug() << OGL << "Mouse moved!";

    GLfloat dx = (event->x() - lastPos.x()) * 0.5f;
    GLfloat dy = (event->y() - lastPos.y()) * 0.5f;
    if (event->buttons() & Qt::LeftButton) {
        setXRotation(xRot + 8 * dy);
        setYRotation(yRot + 8 * dx);
    } else if (event->buttons() & Qt::RightButton) {
        setXRotation(xRot + 8 * dy);
        setZRotation(zRot + 8 * dx);
    } else if (event->buttons() & Qt::MiddleButton) {
        setZRotation(zRot + 8 * dy);
        setYRotation(yRot + 8 * dx);
    }

    lastPos = event->pos();
    updateGL();

}

void GLWidget::keyPressEvent(QKeyEvent *event)
{
    qDebug()<< OGL << "keyPressed!";
    switch(event->key())
    {
    default:
        qDebug() << OGL << "keyPressEvent: Unsupported key: " << event->key();
    }
    updateGL();
}

void GLWidget::wheelEvent(QWheelEvent *event)
{
    qDebug() << OGL << "Scrolling";
    GLfloat numSteps = event->delta() / 15;

    updateGL();
}


static void qNormalizeAngle(int &angle)
{
    while (angle < 0)
        angle += 360 * 16;
    while (angle > 360)
        angle -= 360 * 16;
}

void GLWidget::setXRotation(int angle)
{
    qNormalizeAngle(angle);
    if (angle != xRot) {
        xRot = angle;
        updateGL();
    }
}

void GLWidget::setYRotation(int angle)
{
    qNormalizeAngle(angle);
    if (angle != yRot) {
        yRot = angle;
        updateGL();
    }
}

void GLWidget::setZRotation(int angle)
{
    qNormalizeAngle(angle);
    if (angle != zRot) {
        zRot = angle;
        updateGL();
    }
}
