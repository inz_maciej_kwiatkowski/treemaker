#include "treegraph.h"
#include <QDebug>

TreeGraph::TreeGraph()
{
    root = NULL;
}

TreeGraph::~TreeGraph()
{
    if(root!=NULL)
        delete root;
}

void TreeGraph::addNode()
{
    if(root==NULL)
    {
        printf("root is null. Create NEW TreeNode with axiom %s\n",ProductionMethod::getInstance()->getAxiom().c_str());

        root = new TreeNode(0,ProductionMethod::getInstance()->getAxiom());
    }
    else
    {
        printf("root is not null. Add Node\n");
        root->addNode(ProductionMethod::getInstance()->produceString(root->getProduct()));
    }
}

TreeNode *TreeGraph::getRoot()
{
    return root;
}

std::string TreeGraph::printTree()
{
    printf("printing...\n");
    int i = 1;
    std::string out = "1. ";
    TreeNode* node;
    if(root)
        out += root->getProduct();
        out += "\n";
    while(node = root->getNextNode())
    {
        printf("printing %s...\n",node->getProduct().c_str());
        char buffer[32];
        out += itoa(++i,buffer,10);
        out += ". ";
        out += node->getProduct();
        out += "\n";
    }
    return out;
}

void TreeGraph::increaseTree()
{
    if(!root)
    {
        root = new TreeNode(0,ProductionMethod::getInstance()->getAxiom());
        return;
    }
    preorder(root);
}

void TreeGraph::preorder(TreeNode *node)
{
    switch (node->getBranches())
    {
        case branchesE::TwoBranches :
            if(node->getRightBranch() == NULL)
            {
                node->getRightBranch() = new TreeNode(node,ProductionMethod::getInstance()->produceString(node->getProduct(),0));
            }
            else
            {
                 preorder(node->getRightBranch());
            }
        case branchesE::SingleBranch:
            if(node->getLeftBranch() == NULL)
            {
                node->getLeftBranch() = new TreeNode(node,ProductionMethod::getInstance()->produceString(node->getProduct(),1));
            }
            else
            {
                preorder(node->getLeftBranch());
            }
            break;
        default:
            qDebug() << "Zła wartość w polu 'branches'";
    }
}


