// main.cpp

#include <QApplication>
#include <QDesktopWidget>

#include "window.h"
//#include "treegraph.h"
//#include "productionmethod.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    Window window;
    window.resize(window.sizeHint());
    int desktopArea = QApplication::desktop()->width() *
                     QApplication::desktop()->height();
    int widgetArea = window.width() * window.height();

    window.setWindowTitle("OpenGL with Qt");

    if (((float)widgetArea / (float)desktopArea) < 0.75f)
        window.show();
    else
        window.showMaximized();

   /* TreeGraph tree;
    ProductionMethod* productionMethod = ProductionMethod::getInstance();
    productionMethod.setAxiom("X");
    productionMethod.addProductionString("X->Y");
    productionMethod.addProductionString("Y->X");

    tree.addNode();
    tree.printTree();*/

    return app.exec();
}
