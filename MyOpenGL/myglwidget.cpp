// myglwidget.cpp

#include <QtWidgets>
#include <QtOpenGL>
#include <ctime>
#include "myglwidget.h"

MyGLWidget::MyGLWidget(QWidget *parent)
    : QGLWidget(QGLFormat(QGL::SampleBuffers), parent)
{
    xRot = 0;
    yRot = 0;
    zRot = 0;
}

MyGLWidget::~MyGLWidget()
{
}

QSize MyGLWidget::minimumSizeHint() const
{
    return QSize(50, 50);
}

QSize MyGLWidget::sizeHint() const
{
    return QSize(400, 400);
}

static void qNormalizeAngle(int &angle)
{
    while (angle < 0)
        angle += 360 * 16;
    while (angle > 360)
        angle -= 360 * 16;
}

void MyGLWidget::setXRotation(int angle)
{
    qNormalizeAngle(angle);
    if (angle != xRot) {
        xRot = angle;
        //emit xRotationChanged(angle);
        updateGL();
    }
}

void MyGLWidget::setYRotation(int angle)
{
    qNormalizeAngle(angle);
    if (angle != yRot) {
        yRot = angle;
        //emit yRotationChanged(angle);
        updateGL();
    }
}

void MyGLWidget::setZRotation(int angle)
{
    qNormalizeAngle(angle);
    if (angle != zRot) {
        zRot = angle;
        //emit zRotationChanged(angle);
        updateGL();
    }
}

void MyGLWidget::initializeGL()
{
    initializeOpenGLFunctions();
    qglClearColor(Qt::black);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glShadeModel(GL_SMOOTH);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    static GLfloat lightPosition[4] = { 0, 0, 10, 1.0 };
    glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
/*
    lsystem.fieldOfView = 45;
    lsystem.eyeX = 250;
    lsystem.eyeY = 100;
    lsystem.eyeZ = 100;
    lsystem.lookX = 0;
    lsystem.lookY = 50;
    lsystem.lookZ = 0;
        srand (time(NULL));
    lsystem.num = (float) rand()/RAND_MAX;
*/
/*
        // set the lighting
        glShadeModel(GL_SMOOTH);
        GLfloat lightP[4] = {0.0, 800.0, 0.0,1.0};
        glLightfv(GL_LIGHT0, GL_POSITION, lightP);

        // set the ambient light colour
        GLfloat lightA[4] = {0.0,0.9,0.9,1};
        glLightfv(GL_LIGHT0, GL_AMBIENT, lightA);

        // set the specular light colour
        GLfloat lightS[4] = {0.9,0.9,0.9,1.0};
        glLightfv(GL_LIGHT0, GL_SPECULAR, lightS);

        // set the diffuse light colour
        GLfloat lightD[4] = {0.9,0.9,0.9,1.0};
        glLightfv(GL_LIGHT0, GL_DIFFUSE, lightD);

        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);
*/

        /* Use depth buffering for hidden surface elimination. */
        glEnable(GL_DEPTH_TEST);
}

void MyGLWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glTranslatef(0.0, 0.0, -10.0);
    glRotatef(xRot / 16.0, 1.0, 0.0, 0.0);
    glRotatef(yRot / 16.0, 0.0, 1.0, 0.0);
    glRotatef(zRot / 16.0, 0.0, 0.0, 1.0);
    draw();
}

void MyGLWidget::resizeGL(int width, int height)
{
    int side = qMin(width, height);
    glViewport((width - side) / 2, (height - side) / 2, side, side);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
#ifdef QT_OPENGL_ES_1
    glOrthof(-2, +2, -2, +2, 1.0, 15.0);
#else
    glOrtho(-2, +2, -2, +2, 1.0, 15.0);
#endif
    glMatrixMode(GL_MODELVIEW);
}

void MyGLWidget::mousePressEvent(QMouseEvent *event)
{
    lastPos = event->pos();
}

void MyGLWidget::mouseMoveEvent(QMouseEvent *event)
{
    int dx = event->x() - lastPos.x();
    int dy = event->y() - lastPos.y();

    if (event->buttons() & Qt::LeftButton) {
        setXRotation(xRot + 8 * dy);
        setYRotation(yRot + 8 * dx);
    } else if (event->buttons() & Qt::RightButton) {
        setXRotation(xRot + 8 * dy);
        setZRotation(zRot + 8 * dx);
    }

    lastPos = event->pos();
}

void MyGLWidget::draw()
{
    qglColor(Qt::blue);
/*
    GLUquadricObj* quadric1 = gluNewQuadric();
    gluQuadricDrawStyle(quadric1,GL_FILL) ;
    glColor4f(1.0,0.0,1.0,0.5);

    gluCylinder(quadric1,0.11,0.1,2,20,20);

    glTranslatef(0,0,2);

    GLUquadricObj* quadric = gluNewQuadric();
    gluQuadricDrawStyle(quadric,GL_FILL) ;
    glColor4f(1.0,0.0,1.0,0.5);

    gluCylinder(quadric,0.1,0.09,2,20,20);

    glTranslatef(0,0,2);
    qglColor(Qt::red);
    GLUquadricObj* quadric2 = gluNewQuadric();
    gluQuadricDrawStyle(quadric2,GL_FILL) ;
    glColor4f(0.0,1.0,0.0,0.5);

    gluCylinder(quadric2,0.09,0.001,2,20,20);

    glTranslatef(0,0,2);
*/
    /*
    glBegin(GL_QUADS);
        glNormal3f(0,0,-1);
        glVertex3f(-1,-1,0);
        glVertex3f(-1,1,0);
        glVertex3f(1,1,0);
        glVertex3f(1,-1,0);

    glEnd();
    glBegin(GL_TRIANGLES);
        glNormal3f(0,-1,0.707);
        glVertex3f(-1,-1,0);
        glVertex3f(1,-1,0);
        glVertex3f(0,0,1.2);
    glEnd();
    glBegin(GL_TRIANGLES);
        glNormal3f(1,0, 0.707);
        glVertex3f(1,-1,0);
        glVertex3f(1,1,0);
        glVertex3f(0,0,1.2);
    glEnd();
    glBegin(GL_TRIANGLES);
        glNormal3f(0,1,0.707);
        glVertex3f(1,1,0);
        glVertex3f(-1,1,0);
        glVertex3f(0,0,1.2);
    glEnd();
    glBegin(GL_TRIANGLES);
        glNormal3f(-1,0,0.707);
        glVertex3f(-1,1,0);
        glVertex3f(-1,-1,0);
        glVertex3f(0,0,1.2);
    glEnd();*/
    /*
    int n = 3;
    int arg = 0;
    float mult = 1;
    float v = 1;
    glBegin(GL_QUAD_STRIP);

    glColor4f(1.0, 1.0, 0.0, 1.0);

    for(int i = arg; i < 480; i += (360 / n)) {
        float a = i * M_PI / 180; // degrees to radians
        glVertex3f(mult * cos(a), mult * sin(a), 0.0);
        glVertex3f(mult * cos(a), mult * sin(a), v);
    }

    glEnd();*/

}


void MyGLWidget::produce(){
   // std::string produceMethod = "D[Y]DD";
}
