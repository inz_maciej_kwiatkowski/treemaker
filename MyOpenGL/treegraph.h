#ifndef TREEGRAPH_H
#define TREEGRAPH_H
#include "treenode.h"
#include "productionmethod.h"

class TreeGraph
{
protected:
    TreeNode* root;
public:
    TreeGraph();
    ~TreeGraph();
    void addNode();
    TreeNode* getRoot();
    std::string printTree();
    void increaseTree();
private:
    void preorder(TreeNode* node);
};

#endif // TREEGRAPH_H
