#include "treenode.h"

TreeNode::TreeNode(TreeNode* parent)
{
    printf("Creating Node");
    prev = parent;
    next_left = NULL;
    next_right = NULL;
    lastReturnedNode = NULL;
}

TreeNode::TreeNode(TreeNode *parent, std::string _product)
{
    printf("Creating Node with axiom: %s",_product.c_str());
    prev = parent;
    next_left = NULL;
    next_right = NULL;
    lastReturnedNode = NULL;
    this->product = _product;
}

void TreeNode::addNextLeft(TreeNode *next)
{
    next_left = new TreeNode(next);
}

void TreeNode::addNextRight(TreeNode *next)
{
    next_right = next;
}

void TreeNode::addNextLeft(TreeNode *next, std::string product)
{
    next_left = new TreeNode(next,product);
}

void TreeNode::addNextRight(TreeNode *next, std::string product)
{
    next_right = new TreeNode(next,product);
}

branchesE TreeNode::getBranches()
{
    return branches;
}

void TreeNode::addNode()
{
    if(next_left == NULL)
    {
        addNextLeft(this);
    }
    else if(next_right == NULL)
    {
        addNextRight(this);
    }
}

void TreeNode::addNode(std::string product)
{
    if(next_left == NULL)
    {
        addNextLeft(this, product);
    }
    else if(next_right == NULL)
    {
        addNextRight(this, product);
    }
}

std::string TreeNode::getProduct()
{
    return this->product;
}

TreeNode *TreeNode::getNextNode()
{
    if(lastReturnedNode == NULL)
    {
        if(next_left != NULL )
        {
            lastReturnedNode = next_left;
        }
    }
    else if(lastReturnedNode == next_left)
    {
        if(next_right != NULL)
        {
            lastReturnedNode = next_right;
        }
        else
        {
            lastReturnedNode = NULL;
        }
    }
    return lastReturnedNode;
}

TreeNode*& TreeNode::getLeftBranch()
{
    return next_left;
}

TreeNode*& TreeNode::getRightBranch()
{
    return next_right;
}
