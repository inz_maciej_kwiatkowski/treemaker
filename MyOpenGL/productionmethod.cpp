#include "productionmethod.h"
#include <QDebug>

ProductionMethod* ProductionMethod::instance;
ProductionMethod::ProductionMethod(char axiom, LAlphabet alphabet)
{
    printf("/\\/\\/\\Creating Instance of ProdutionMethod!/\\/\\/\\");
}

void ProductionMethod::setAxiom(std::string axiom)
{
    instance->axiom=axiom;
}

void ProductionMethod::addProductionString(std::string productionStr)
{
    std::size_t pos = productionStr.find("->");
    printf("Tree: pos %d\n",pos);
    std::string axiom = productionStr.substr(0,pos);
    printf("Tree: axiom %s\n",axiom.c_str());
    std::string product = productionStr.substr(pos+2);
    printf("Tree: product %s\n",product.c_str());
    instance->productionMap[axiom] = product;// = product;
}

bool ProductionMethod::produce(std::string axiom, std::string& product)
{
    auto it = instance->productionMap.find(axiom);
    if(it != instance->productionMap.end())
    {
        product = it->second;
        return true;
    }
    product = "";
    return false;
}

int ProductionMethod::produce(std::string axiom)
{
    int product = (int)ProductE::NOT_VALID;
    auto it = instance->productionMap.find(axiom);
    if(it != instance->productionMap.end())
    {
        std::string productString = it->second;
        if(productString.length() == 1)
        {
            product += (int)mapSign((char)productString[0]);
        }

    }
    return product;
}

std::string ProductionMethod::produceString(std::string axiom, int branchIndex)
{
    int product = (int)ProductE::NOT_VALID;
    auto it = instance->productionMap.find(axiom);
    if(it != instance->productionMap.end())
    {
        if(branchIndex == -1)
            return it->second;
        else
        {
            return devideString(it->second,branchIndex);
        }

    }
    return "product";
}

std::string ProductionMethod::devideString(std::string str, int stringPart)
{
    std::size_t posX = str.find("X");
    std::size_t posY = str.find("Y");
    std::size_t posI = str.find("I");
    qDebug() << "posX: " << posX << ", posY: " << posY << ", posI: " << posI;
    return "product";
}

ProductE ProductionMethod::mapSign(char sign)
{
    switch(sign){
    case 'X': return ProductE::BRANCH;
    case 'Y': return ProductE::CREATE_TWO_NODES;
    case 'I': return ProductE::CREATE_ONE_NODE;
    default : return ProductE::NOT_VALID;
    }
}

std::string ProductionMethod::printAlphabet()
{
    return "Nic tu nie ma";
}

std::string ProductionMethod::printProductionRules()
{
    return "Nic tu nie ma";
}


ProductionMethod* ProductionMethod::getInstance()
{
    if(!instance)
        instance = new ProductionMethod();
    return instance;
}

ProductionMethod::~ProductionMethod()
{
    /*if(ProductionMethod::instance!=NULL)
        delete ProductionMethod::instance;
*/}

/*std::string ProductionMethod::produceNextNode(TreeNode parentNode)
{
    std::string nodeProduct = parentNode.getProduct();
    return productionMap[nodeProduct];
}*/

char ProductionMethod::getAxiom()
{
    printf("Returning axiom: %s",axiom);
    return instance->axiom;
}
