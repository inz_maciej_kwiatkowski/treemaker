#ifndef TREENODE_H
#define TREENODE_H
#include <string>

enum branchesE
{
    NotValid = 0,
    SingleBranch = 1,
    TwoBranches  = 2
};

//based on bit tree
class TreeNode
{
protected:
    TreeNode* prev;
    TreeNode* next_left;
    TreeNode* next_right;
    std::string product;
    int rotateX;
    int rotateY;
    int rotateZ;
    branchesE branches;
    bool fullyProduced;
    TreeNode* lastReturnedNode;
public:
    TreeNode(TreeNode* parent);
    TreeNode(TreeNode* parent, std::string _product);
    void addNextLeft(TreeNode* next);
    void addNextRight(TreeNode* next);
    void addNode();
    void addNode(std::string);
    std::string getProduct();
    void setBranches(branchesE branches);
    TreeNode* getNextNode();
    TreeNode*& getLeftBranch();
    TreeNode*& getRightBranch();
    void addNextLeft(TreeNode *next, std::string product);
    void addNextRight(TreeNode *next, std::string product);
    branchesE getBranches();
};

#endif // TREENODE_H
