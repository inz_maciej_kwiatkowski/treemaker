#ifndef PRODUCTIONMETHOD_H
#define PRODUCTIONMETHOD_H
#include <string>
#include <map>
//#include "treenode.h"

enum class ProductE{
    NOT_VALID =        0,
    BRANCH  =          1,   //X
    CREATE_ONE_NODE =  2,   //I
    CREATE_TWO_NODES = 4,   //Y
};

class ProductionMethod
{
protected:
    std::map<std::string, std::string> productionMap;
    std::string axiom = "";
private:
    ProductionMethod();
    static ProductionMethod* instance;
public:
    void setAxiom(std::string axiom);
    void addProductionString(std::string productionStr);
    bool produce(std::string axiom, std::string& product);
    int produce(std::string axiom);
    ProductE mapSign(char sign);
    std::string printAlphabet();
    std::string printProductionRules();
    static ProductionMethod *getInstance();
    ~ProductionMethod();
    //std::string produceNextNode(TreeNode parentNode);
    //bool generateNode(TreeNode& parentNode);
    std::string getAxiom();

    // branchIndex służy określeniu dla której branchy chcemy wyprodukować łańcuch znaków
    // domyślne -1 zwraca całe słowo
    // wartość 0 zwraca pierwszą wartość w indeksie itd..
    std::string produceString(std::string axiom, int branchIndex = -1);
    std::string devideString(std::string str, int stringPart);
};

#endif // PRODUCTIONMETHOD_H
