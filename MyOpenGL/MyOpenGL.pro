#-------------------------------------------------
#
# Project created by QtCreator 2013-09-25T09:11:42
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MyOpenGL
TEMPLATE = app

LIBS += opengl32.lib
LIBS += glu32.lib

SOURCES += main.cpp\
        window.cpp \
    myglwidget.cpp \
    treenode.cpp \
    treegraph.cpp \
    productionmethod.cpp

HEADERS  += window.h \
    myglwidget.h \
    treenode.h \
    treegraph.h \
    productionmethod.h

FORMS    += window.ui
