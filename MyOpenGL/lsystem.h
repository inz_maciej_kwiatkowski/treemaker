#ifndef LSYSTEM_H
#define LSYSTEM_H

//#include <GL/glut.h>
//#include <gl/GL.h>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <math.h>
#include <time.h>


class LSystem
{
public:
    const float PI = 3.14, DEPTH = 6;
    // Start and end of camera movement
    const int ENDX = 10, STARTX = -500;
    // Angle of branches, and depth of tree
    float ANGLE = 20, depth = 0;
    std::vector<std::string> *trees;


    double lastTime = 0, elapsedTime = 0, lastElapsedTime = 0;

    bool cam = false;

    float eyeX, eyeY, eyeZ, lookX, lookY, lookZ,
        upX, upY, upZ, fieldOfView, length = 0.001, num = 0,
            incr = 0.1;

    float lineWidth = 5;
    // L-System
    std::string str = "X";
public:
    LSystem();
    void push();
    void pop();
    void rotL();
    void rotR();
    void leaf();
    void drawLine();
    void draw();
    void expand(float num);
    void display();
    void animate();
    void keyboard(unsigned char key, int x, int y);
};

#endif // LSYSTEM_H
