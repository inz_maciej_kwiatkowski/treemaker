/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "glwidget.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout_8;
    GLWidget *widget;
    QVBoxLayout *verticalLayout_5;
    QGroupBox *alphabetBox;
    QVBoxLayout *verticalLayout_6;
    QTextBrowser *textBrowser;
    QGroupBox *productionBox;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label;
    QLineEdit *lineEdit;
    QVBoxLayout *verticalLayout_2;
    QLabel *rulesLabel;
    QHBoxLayout *horizontalLayout_10;
    QLabel *LLabel;
    QLineEdit *LEdit;
    QHBoxLayout *horizontalLayout_11;
    QLabel *BLable;
    QLineEdit *BEdit;
    QHBoxLayout *horizontalLayout_12;
    QLabel *gLable;
    QLineEdit *gEdit;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *useRuleButton;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *changeRuleButton;
    QGroupBox *randomnessBox;
    QVBoxLayout *verticalLayout_4;
    QCheckBox *checkBox;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_4;
    QSlider *horizontalSlider_2;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_5;
    QSlider *horizontalSlider_3;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_3;
    QSlider *horizontalSlider;
    QGroupBox *generationBox;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_9;
    QLabel *iterationLabel;
    QSpinBox *iterationSpin;
    QSpacerItem *horizontalSpacer_4;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *generateButton;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *eksportButton;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *quiteButton;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->setWindowModality(Qt::ApplicationModal);
        MainWindow->resize(1071, 761);
        MainWindow->setAutoFillBackground(false);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        horizontalLayout_8 = new QHBoxLayout(centralWidget);
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        widget = new GLWidget(centralWidget);
        widget->setObjectName(QStringLiteral("widget"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(widget->sizePolicy().hasHeightForWidth());
        widget->setSizePolicy(sizePolicy);
        widget->setMinimumSize(QSize(400, 600));

        horizontalLayout_8->addWidget(widget);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        verticalLayout_5->setSizeConstraint(QLayout::SetDefaultConstraint);
        alphabetBox = new QGroupBox(centralWidget);
        alphabetBox->setObjectName(QStringLiteral("alphabetBox"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Maximum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(alphabetBox->sizePolicy().hasHeightForWidth());
        alphabetBox->setSizePolicy(sizePolicy1);
        alphabetBox->setMinimumSize(QSize(0, 200));
        verticalLayout_6 = new QVBoxLayout(alphabetBox);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        textBrowser = new QTextBrowser(alphabetBox);
        textBrowser->setObjectName(QStringLiteral("textBrowser"));

        verticalLayout_6->addWidget(textBrowser);


        verticalLayout_5->addWidget(alphabetBox);

        productionBox = new QGroupBox(centralWidget);
        productionBox->setObjectName(QStringLiteral("productionBox"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(productionBox->sizePolicy().hasHeightForWidth());
        productionBox->setSizePolicy(sizePolicy2);
        verticalLayout_3 = new QVBoxLayout(productionBox);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label = new QLabel(productionBox);
        label->setObjectName(QStringLiteral("label"));
        QSizePolicy sizePolicy3(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy3);

        horizontalLayout_3->addWidget(label);

        lineEdit = new QLineEdit(productionBox);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        QSizePolicy sizePolicy4(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(lineEdit->sizePolicy().hasHeightForWidth());
        lineEdit->setSizePolicy(sizePolicy4);

        horizontalLayout_3->addWidget(lineEdit);


        verticalLayout_3->addLayout(horizontalLayout_3);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        rulesLabel = new QLabel(productionBox);
        rulesLabel->setObjectName(QStringLiteral("rulesLabel"));

        verticalLayout_2->addWidget(rulesLabel);


        verticalLayout_3->addLayout(verticalLayout_2);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        LLabel = new QLabel(productionBox);
        LLabel->setObjectName(QStringLiteral("LLabel"));

        horizontalLayout_10->addWidget(LLabel);

        LEdit = new QLineEdit(productionBox);
        LEdit->setObjectName(QStringLiteral("LEdit"));

        horizontalLayout_10->addWidget(LEdit);


        verticalLayout_3->addLayout(horizontalLayout_10);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(6);
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        BLable = new QLabel(productionBox);
        BLable->setObjectName(QStringLiteral("BLable"));

        horizontalLayout_11->addWidget(BLable);

        BEdit = new QLineEdit(productionBox);
        BEdit->setObjectName(QStringLiteral("BEdit"));

        horizontalLayout_11->addWidget(BEdit);


        verticalLayout_3->addLayout(horizontalLayout_11);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setSpacing(6);
        horizontalLayout_12->setObjectName(QStringLiteral("horizontalLayout_12"));
        gLable = new QLabel(productionBox);
        gLable->setObjectName(QStringLiteral("gLable"));

        horizontalLayout_12->addWidget(gLable);

        gEdit = new QLineEdit(productionBox);
        gEdit->setObjectName(QStringLiteral("gEdit"));

        horizontalLayout_12->addWidget(gEdit);


        verticalLayout_3->addLayout(horizontalLayout_12);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        useRuleButton = new QPushButton(productionBox);
        useRuleButton->setObjectName(QStringLiteral("useRuleButton"));

        horizontalLayout_4->addWidget(useRuleButton);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_2);

        changeRuleButton = new QPushButton(productionBox);
        changeRuleButton->setObjectName(QStringLiteral("changeRuleButton"));

        horizontalLayout_4->addWidget(changeRuleButton);


        verticalLayout_3->addLayout(horizontalLayout_4);


        verticalLayout_5->addWidget(productionBox);

        randomnessBox = new QGroupBox(centralWidget);
        randomnessBox->setObjectName(QStringLiteral("randomnessBox"));
        sizePolicy2.setHeightForWidth(randomnessBox->sizePolicy().hasHeightForWidth());
        randomnessBox->setSizePolicy(sizePolicy2);
        verticalLayout_4 = new QVBoxLayout(randomnessBox);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        checkBox = new QCheckBox(randomnessBox);
        checkBox->setObjectName(QStringLiteral("checkBox"));
        checkBox->setCheckable(false);

        verticalLayout_4->addWidget(checkBox);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        label_4 = new QLabel(randomnessBox);
        label_4->setObjectName(QStringLiteral("label_4"));
        QSizePolicy sizePolicy5(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy5.setHorizontalStretch(0);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(label_4->sizePolicy().hasHeightForWidth());
        label_4->setSizePolicy(sizePolicy5);

        horizontalLayout_6->addWidget(label_4);

        horizontalSlider_2 = new QSlider(randomnessBox);
        horizontalSlider_2->setObjectName(QStringLiteral("horizontalSlider_2"));
        sizePolicy2.setHeightForWidth(horizontalSlider_2->sizePolicy().hasHeightForWidth());
        horizontalSlider_2->setSizePolicy(sizePolicy2);
        horizontalSlider_2->setOrientation(Qt::Horizontal);

        horizontalLayout_6->addWidget(horizontalSlider_2);


        verticalLayout_4->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        label_5 = new QLabel(randomnessBox);
        label_5->setObjectName(QStringLiteral("label_5"));
        sizePolicy5.setHeightForWidth(label_5->sizePolicy().hasHeightForWidth());
        label_5->setSizePolicy(sizePolicy5);

        horizontalLayout_7->addWidget(label_5);

        horizontalSlider_3 = new QSlider(randomnessBox);
        horizontalSlider_3->setObjectName(QStringLiteral("horizontalSlider_3"));
        sizePolicy2.setHeightForWidth(horizontalSlider_3->sizePolicy().hasHeightForWidth());
        horizontalSlider_3->setSizePolicy(sizePolicy2);
        horizontalSlider_3->setOrientation(Qt::Horizontal);

        horizontalLayout_7->addWidget(horizontalSlider_3);


        verticalLayout_4->addLayout(horizontalLayout_7);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        label_3 = new QLabel(randomnessBox);
        label_3->setObjectName(QStringLiteral("label_3"));
        sizePolicy5.setHeightForWidth(label_3->sizePolicy().hasHeightForWidth());
        label_3->setSizePolicy(sizePolicy5);

        horizontalLayout_5->addWidget(label_3);

        horizontalSlider = new QSlider(randomnessBox);
        horizontalSlider->setObjectName(QStringLiteral("horizontalSlider"));
        sizePolicy2.setHeightForWidth(horizontalSlider->sizePolicy().hasHeightForWidth());
        horizontalSlider->setSizePolicy(sizePolicy2);
        horizontalSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_5->addWidget(horizontalSlider);


        verticalLayout_4->addLayout(horizontalLayout_5);


        verticalLayout_5->addWidget(randomnessBox);

        generationBox = new QGroupBox(centralWidget);
        generationBox->setObjectName(QStringLiteral("generationBox"));
        sizePolicy2.setHeightForWidth(generationBox->sizePolicy().hasHeightForWidth());
        generationBox->setSizePolicy(sizePolicy2);
        verticalLayout = new QVBoxLayout(generationBox);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        iterationLabel = new QLabel(generationBox);
        iterationLabel->setObjectName(QStringLiteral("iterationLabel"));
        sizePolicy3.setHeightForWidth(iterationLabel->sizePolicy().hasHeightForWidth());
        iterationLabel->setSizePolicy(sizePolicy3);

        horizontalLayout_9->addWidget(iterationLabel);

        iterationSpin = new QSpinBox(generationBox);
        iterationSpin->setObjectName(QStringLiteral("iterationSpin"));
        iterationSpin->setMinimum(2);

        horizontalLayout_9->addWidget(iterationSpin);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_4);


        verticalLayout->addLayout(horizontalLayout_9);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        generateButton = new QPushButton(generationBox);
        generateButton->setObjectName(QStringLiteral("generateButton"));
        QSizePolicy sizePolicy6(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy6.setHorizontalStretch(0);
        sizePolicy6.setVerticalStretch(0);
        sizePolicy6.setHeightForWidth(generateButton->sizePolicy().hasHeightForWidth());
        generateButton->setSizePolicy(sizePolicy6);

        horizontalLayout_2->addWidget(generateButton);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);

        eksportButton = new QPushButton(generationBox);
        eksportButton->setObjectName(QStringLiteral("eksportButton"));
        sizePolicy6.setHeightForWidth(eksportButton->sizePolicy().hasHeightForWidth());
        eksportButton->setSizePolicy(sizePolicy6);

        horizontalLayout_2->addWidget(eksportButton);


        verticalLayout->addLayout(horizontalLayout_2);


        verticalLayout_5->addWidget(generationBox);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        quiteButton = new QPushButton(centralWidget);
        quiteButton->setObjectName(QStringLiteral("quiteButton"));
        sizePolicy6.setHeightForWidth(quiteButton->sizePolicy().hasHeightForWidth());
        quiteButton->setSizePolicy(sizePolicy6);

        horizontalLayout->addWidget(quiteButton);


        verticalLayout_5->addLayout(horizontalLayout);


        horizontalLayout_8->addLayout(verticalLayout_5);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1071, 20));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "TreeMaker", 0));
        alphabetBox->setTitle(QApplication::translate("MainWindow", "Alfabet:", 0));
        textBrowser->setHtml(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">L: nowa, m\305\202oda ga\305\202\304\205\305\272 (mo\305\274e posiada\304\207 li\305\233cie i owoce)</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">B: starsza ga\305\202\304\205\305\272 (nie posiada li\305\233ci)</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">g: zwi\304\231kszenie wielko\305\233ci ga\305\202\304\231zi</p></body></html>", 0));
        productionBox->setTitle(QApplication::translate("MainWindow", "Metody produkcji", 0));
        label->setText(QApplication::translate("MainWindow", "Axiom:", 0));
        lineEdit->setText(QApplication::translate("MainWindow", "L", 0));
        rulesLabel->setText(QApplication::translate("MainWindow", "Metody produkcji:", 0));
        LLabel->setText(QApplication::translate("MainWindow", "L->", 0));
        LEdit->setText(QApplication::translate("MainWindow", "BLL", 0));
        BLable->setText(QApplication::translate("MainWindow", "B->", 0));
        BEdit->setText(QApplication::translate("MainWindow", "gB", 0));
        gLable->setText(QApplication::translate("MainWindow", "g->", 0));
        useRuleButton->setText(QApplication::translate("MainWindow", "U\305\274yj metod", 0));
        changeRuleButton->setText(QApplication::translate("MainWindow", "Zmie\305\204 metody", 0));
        randomnessBox->setTitle(QApplication::translate("MainWindow", "Losowo\305\233\304\207", 0));
        checkBox->setText(QApplication::translate("MainWindow", "za\305\202\304\205cz losowo\305\233\304\207", 0));
        label_4->setText(QApplication::translate("MainWindow", "z:", 0));
        label_5->setText(QApplication::translate("MainWindow", "y:", 0));
        label_3->setText(QApplication::translate("MainWindow", "x:", 0));
        generationBox->setTitle(QApplication::translate("MainWindow", "Generacja", 0));
        iterationLabel->setText(QApplication::translate("MainWindow", "Ilo\305\233\304\207 iteracji:", 0));
        generateButton->setText(QApplication::translate("MainWindow", "Generuj", 0));
        eksportButton->setText(QApplication::translate("MainWindow", "Eksport", 0));
        quiteButton->setText(QApplication::translate("MainWindow", "&Quit", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
